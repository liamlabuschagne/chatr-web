# Chatr Website/Server #
This is the repo for the website and server half of the chatr app.

## Description ##
This app will allow for extremely low data-usage text messaging using an encrytped tunnel end-to-end and data on each mobile device will be stored in encrypted files.
The ultimate goal of this app is to provide SMS-like functionality for a fraction of the price of casual rates texting, monthly plan costs or even popular internet messaging apps by sending only the bare-minimum information using mobile data. 

## The Cost Savings ##
For example, the casual rates for my mobile provider is 10c per text and 20c per MB. A 250 character message only uses 250 bytes of data rounded up for http headers etc we can say is 1KB which is 1/1000 of a MB so is 0.02/1000 = $0.00002 per message! This is equivalent to 500 messages for 10c if using regular SMS!

## Authentication and Privacy ##
Users will be identified on the server only by a randomly generated user id and password on-download and messages will be stored in ram until read from the server, at which point the message will only remain on the sender and receiver's mobile device. Users will only be able to send messages to those who they have added to their locally stored contacts by scanning each others QR code in a face-to-face confrontation.

## Summary of Features
* Fully Encrypted
* Fully Anonymous
* Cheapest Possible Communication
* Fully Open-Source (You can run your own servers)